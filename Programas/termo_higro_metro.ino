#include "DHT.h"
#define DHTPIN 2

#define DHTTYPE DHT11
//#define DHTTYPE DHT22
//#define DHTTYPE DHT21

DHT dht(DHTPIN, DHTTYPE);

void setup() {
  Serial.begin(9600);
  dht.begin();
}

void loop() {
Serial.print("Temperatura: ");
Serial.print(dht.readTemperature());
Serial.println("ºC");
Serial.print("Humedad: ");
Serial.print(dht.readHumidity());
Serial.println("%");
delay(2000);
}
