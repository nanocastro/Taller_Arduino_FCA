#define sueloPin A0
#define alimSuelo 9
#define inMin 0
#define inMax 1023
#define outMin 0
#define outMax 100

void setup() {
pinMode(alimSuelo, OUTPUT);
}

void loop() {
  digitalWrite(alimSuelo, HIGH);
  delay(1000);
  Serial.println(analogRead(sueloPin));
  Serial.println(remap(analogRead(sueloPin), inMin, inMax, outMin, outMax));
  digitalWrite(alimSuelo, LOW);
  delay (2000);
}

float remap(float x, float in_min, float in_max, float out_min, float out_max)
{
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

