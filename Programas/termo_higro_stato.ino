#include "DHT.h"
#include "EEPROM.h"
#define DHTPIN 2
#define calPin 8
#define humPin 9

#define DHTTYPE DHT11
//#define DHTTYPE DHT22
//#define DHTTYPE DHT21

DHT dht(DHTPIN, DHTTYPE);

float H = 0;
float T = 0;
float Tset = 0;
float Hset = 0;
unsigned long prevMillis = 0;

void setup() {
  pinMode(calPin, OUTPUT);
  pinMode(humPin, OUTPUT);
  Serial.begin(9600);
  EEPROM.get(0, Tset);
  EEPROM.get(4, Hset);
  dht.begin();
}

void loop() {
  delay(100);

  T = dht.readTemperature();
  H = dht.readHumidity();

  if (T < Tset)
  {
    digitalWrite(calPin, HIGH);
  }
  else
  {
    digitalWrite(calPin, LOW);
  }

  if (H < Hset)
  {
    digitalWrite(humPin, HIGH);
  }
  else
  {
    digitalWrite(humPin, LOW);
  }

  if (millis() >= prevMillis + 2000)
  {
    prevMillis = millis();
    Serial.print("Temperatura: ");
    Serial.print(T);
    Serial.print("/");
    Serial.print(Tset);
    Serial.print("ºC \t");
    Serial.println(digitalRead(calPin));
    Serial.print("Humedad: ");
    Serial.print(H);
    Serial.print("/");
    Serial.print(Hset);
    Serial.print("% \t");
    Serial.println(digitalRead(humPin));
  }
  
  if (Serial.available())
  {
    String entrada = Serial.readString();
    String aux;
    float temp = 0;
    float hum = 0;

    aux = entrada.substring(0, entrada.indexOf(' '));
    temp = aux.toFloat();
    Serial.print("Valores recibidos: ");
    Serial.print(temp);
    Serial.print("\t");
    aux = entrada.substring(entrada.indexOf(' ') + 1);
    hum = aux.toFloat();
    Serial.println(hum);
    Serial.println("Guardar? S/N");
    while (1)
    {
      if (Serial.read() == 'S')
      {
        EEPROM.put(0, temp);
        EEPROM.put(4, hum);
        Tset = temp;
        Hset = hum;
        Serial.println("Valores guardados");
        break;
      }
      else if (Serial.read() == 'N')
      {
        Serial.println("Valores descartados");
        break;
      }
    }
  }
}
