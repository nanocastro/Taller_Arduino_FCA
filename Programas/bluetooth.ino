#define LED 13
int estado = 0;
int demora = 1000;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(38400);
  pinMode(LED, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available())
  {
    char a  = Serial.read();
    switch (a)
    {
      case 'E':
        {
          estado = 1;
        }
        break;

      case 'A':
        {
          estado = 0;
        }
        break;
      case 'P':
        {
          if (Serial.available())
          {
            estado = 2;
            demora = Serial.readString().toInt();
          }
        }
        break;
      default:
        Serial.println("QUE QUE?!");
        break;

    }

  }
  switch (estado)
  {
    case 0:
      digitalWrite(LED, LOW);
      break;

    case 1:
      digitalWrite(LED, HIGH);
      break;

    case 2:
      {
        digitalWrite(LED, LOW);
        delay(demora);
        digitalWrite(LED, HIGH);
        delay(demora);
      }
      break;
    default:
      break;
  }
}
