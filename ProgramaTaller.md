#  /////Taller Introductorio de Arduino para Agronomía/////

# **2018**


# Presentación

La posibilidad de diseñar, fabricar y mejorar el instrumental científico es parte fundamental del proceso de investigación y desarrollo tecnológico. Estas actividades están restringidas en aquellos instrumentos patentados que no permiten acceso a la documentación de diseño y funcionamiento. Esto impide inspeccionar, evaluar o adaptar estos dispositivos a las necesidades y saberes particulares de cada laboratorio o comunidad. Esta situación perjudica de forma directa la producción de conocimiento y el desarrollo tecnológico, limitando además el potencial de generación de soluciones equitativas y sostenibles en respuesta a problemas locales.

El Hardware Científico Abierto (HCA) surge emulando la filosofía y la ética del software abierto y se refiere a cualquier pieza de hardware que pueda ser obtenida, ensamblada, usada, estudiada, modificada, compartida y comercializada por cualquier persona. Incluye equipos estándar de laboratorio, tales como sensores, microscopios y termocicladores, así también como dispositivos diseñados _ad hoc_ para diseños experimentales particulares (ie. [https://channels.plos.org/open-source-toolkit](https://channels.plos.org/open-source-toolkit)) . 

El auge de esto dispositivos científicos abiertos está, por lo tanto, estrechamente relacionado con la documentación y la diseminación de sus diseños digitales y con la inherente posibilidad de modificarlos y adaptarlos a diferentes contextos y necesidades. Sin embargo, esta potencia sólo puede ser materializada por quienes manejan las plataformas y herramientas de prototipado rápido que han permitido y acompañan el desarrollo del HCA (ie. impresión 3D, Arduino, Git). 


# Objetivos del taller

Los objetivos de este _Taller Introductorio de Arduino para Agronomía_ son:



*   introducir a los participantes en el uso de la plataforma de prototipado electrónico Arduino guiándolos en la construcción de distintos dispositivos lúdicos y científicos.
*   presentar y promover los proyectos y la [comunidad de HCA global](http://openhardware.science/) en Mendoza 
*   relevar las necesidades concretas de la comunidad científica para acompañar y facilitar la construcción y documentación de prototipos de HCA específicos
*   conectar a los actores locales interesados en el desarrollo, el uso y la proliferación de estas tecnologías para fortalecer a las comunidades de aprendizaje y las infraestructuras que les dan sustento. 


# Duración

18 horas aprox. 


# Formato

Cuatro encuentros de 4 horas (o 5 encuentros de 3 horas). Se consideran dos horas adicionales de tutoría para proyecto final. Existe la posibilidad de ampliar los contenidos y el número de encuentros según se requiera.


# Programa

## Encuentro 1. Abriendo la caja negra. Introducción a la electrónica


* Contenidos_: Nociones básicas de electrónica. Materiales conductivos y aislantes. Nociones de voltaje, corriente y resistencia. Ley de Ohm. Corriente alterna y corriente continua. 
* Proyectos_: abordaje lúdico y creativo mediante reutilización de componentes. Uso de soldador de estaño y tester. 
* Materiales_: RAEE, soldador de estaño, tester.

## Encuentro 2. Hola mundo. Introducción Arduino


* Contenidos_: Presentación general de Arduino (Software y Hardware). Configuración de comunicación entre placa e IDE (ambiente integrado de desarrollo). Entradas y salidas digitales. Estructura de control. 
* Proyectos_: LED: Control de una salida digital. Botón: Control de una entrada digital. Semáforo.
* Materiales_: Arduino UNO, botón, leds y resistencias.

## Encuentro 3. Adquiriendo datos del entorno. Sensores analógicos


* Contenidos_: Entradas analógicas: teoría básica de divisores resistivos y definición de ADC (conversores analógico a digital). Sensores: Potenciómetros, LDRs, DHT11.
* Proyectos_: Pote: Entrada analógica y monitor serie. Fotómetro sonoro: Control de una salida digital según entrada analógica. Sensor DHT11: Medidas de temperatura y humedad. Sensor humedad de suelo.
* Materiales_: DHT11, sensor humedad suelo, potenciómetro, buzzer, LDR.

## Encuentro 4. Haciendo visibles y almacenando datos. Display y datalogging 


* Contenidos_: Visualización de datos en display y por pantalla, almacenamiento en PC y memoria SD. Uso de reloj de tiempo real. 
* Proyectos_: Display LCD: Configuración de un display LCD para mostrar varias medidas y mensajes. Display con Processing 3.0. RTC: timestamp para datos. Datalogger: Almacenamiento de datos en tarjeta de memoria externa.
* Materiales_: pantalla LCD

## Encuentro 5 (optativo). Comunicación

* Contenidos: transmisión de los datos obtenidos mediante bluetooth y wi-fi.

## Encuentro 6 (optativo). Actuadores

* Contenidos: introducción al manejo de motores paso a paso y uso de relays.

## Proyecto final


    El desafío de este proyecto es que los participantes puedan integrar los conocimientos y experiencias adquiridas durante el taller en la construcción y documentación de un prototipo científico o educativo. Este prototipo será diseñado en grupo durante los distintos encuentros y a partir sus propias necesidades e inquietudes.


# Talleristas

_LabFD UTN-FRM. Fernando Castro_  
_Santiago Bari_  
_Ana Paula Gei_  

Docente responsable del Laboratorio de Fabricación Digital ([LabFD](http://www.frm.utn.edu.ar/labfd)) de la Universidad Tecnológica Nacional - Regional Mendoza. Trabaja con herramientas de prototipado rápido en el diseño y la construcciòn de hardware científico y educativo abierto de bajo costo. Actualmente está involucrado en el desarrollo del [MACA](http://www.monitorabierto.wikidot.com), un monitor de calidad del aire abierto, y de un set de herramientas educativas para l[aboratorios de física](http://www.frm.utn.edu.ar/labfd/?q=node/3 ) .


# Lista de Materiales

El material puede ser adquirido directamente en el proveedor local o puede ser provisto por los talleristas al costo mencionado más abajo.

Es necesario contar al menos con un kit cada 3 personas.


<table>
  <tr>
   <td><strong>Componente/Sensor</strong>
   </td>
   <td><strong>Link</strong>
   </td>
  </tr>
  <tr>
   <td>Cables con conectores Dupont x 30 (para todos los asistentes). Conectores dupont. M-H, H-H y M-M
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/4031-40-cables-hembra-hembra-10cm-premium-dupont-arduino-itytarg.html?search_query=cables&results=14">http://tienda.ityt.com.ar/mercadolibre/4031-40-cables-hembra-hembra-10cm-premium-dupont-arduino-itytarg.html?search_query=cables&results=14</a>
<p>
<a href="http://tienda.ityt.com.ar/mercadolibre/2970-40-cables-macho-hembra-20cm-premium-dupont-arduino-itytarg.html?search_query=cables&results=14">http://tienda.ityt.com.ar/mercadolibre/2970-40-cables-macho-hembra-20cm-premium-dupont-arduino-itytarg.html?search_query=cables&results=14</a>
   </td>
  </tr>
  <tr>
   <td>Resistencias varias (1k,10k,220 ohm)
   </td>
   <td>Proveerán los talleristas
   </td>
  </tr>
  <tr>
   <td>Arduino UNO R3
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/764-arduino-uno-r3-mega328p.html">http://tienda.ityt.com.ar/mercadolibre/764-arduino-uno-r3-mega328p.html</a>
   </td>
  </tr>
  <tr>
   <td>Sensor humedad de suelo 
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/3069-sensor-humedad-suelo-moisture-higrometro-arduino-itytarg.html?search_query=humedad&results=30">http://tienda.ityt.com.ar/mercadolibre/3069-sensor-humedad-suelo-moisture-higrometro-arduino-itytarg.html?search_query=humedad&results=30</a> 
   </td>
  </tr>
  <tr>
   <td>Modulo Micro Sd Card 5v Con Adaptador 3v3 Arduino
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/194-modulo-micro-sd-card-5v-con-adaptador-3v3-arduino-itytarg.html?search_query=sd+card&results=20">http://tienda.ityt.com.ar/mercadolibre/194-modulo-micro-sd-card-5v-con-adaptador-3v3-arduino-itytarg.html?search_query=sd+card&results=20</a>
   </td>
  </tr>
  <tr>
   <td>DHT11
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/2143-dht11-sensor-temperatura-humedad-arduino-itytarg.html?search_query=dht11&results=6">http://tienda.ityt.com.ar/mercadolibre/2143-dht11-sensor-temperatura-humedad-arduino-itytarg.html?search_query=dht11&results=6</a> 
   </td>
  </tr>
  <tr>
   <td>LDR
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/119-ky-018-foto-resistor-ldr-modulo-arduino-itytarg.html?search_query=ldr&results=5">http://tienda.ityt.com.ar/mercadolibre/119-ky-018-foto-resistor-ldr-modulo-arduino-itytarg.html?search_query=ldr&results=5</a> 
   </td>
  </tr>
  <tr>
   <td>Buzzer
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/188-ky-006-modulo-buzzer-pasivo-5v-arduino-itytarg.html?search_query=buzzer&results=20">http://tienda.ityt.com.ar/mercadolibre/188-ky-006-modulo-buzzer-pasivo-5v-arduino-itytarg.html?search_query=buzzer&results=20</a> 
   </td>
  </tr>
  <tr>
   <td>Protoboard/Breadboard
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/1919-protoboard-breadboard-placa-experimental-400-puntos-itytarg.html?search_query=breadboard&results=7">http://tienda.ityt.com.ar/mercadolibre/1919-protoboard-breadboard-placa-experimental-400-puntos-itytarg.html?search_query=breadboard&results=7</a> 
   </td>
  </tr>
  <tr>
   <td>RTC
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/4844-modulo-reloj-rtc-ds1302-arduino-itytarg.html?search_query=rtc&results=14">http://tienda.ityt.com.ar/mercadolibre/4844-modulo-reloj-rtc-ds1302-arduino-itytarg.html?search_query=rtc&results=14</a> 
   </td>
  </tr>
  <tr>
   <td>Display LCD 16x2
   </td>
   <td><a href="http://tienda.ityt.com.ar/mercadolibre/531-display-lcd-16x2-verde-fordata-fdcc1602e-itytarg.html?search_query=display&results=53">http://tienda.ityt.com.ar/mercadolibre/531-display-lcd-16x2-verde-fordata-fdcc1602e-itytarg.html?search_query=display&results=53</a>
   </td>
  </tr>
</table>


También se necesitan estaño, [soldadores de estaño](https://articulo.mercadolibre.com.ar/MLA-678029380-soldador-lapiz-60-watts-estano-electronica-punta-titanium-_JM), destornilladores, pinzas y [tester](https://articulo.mercadolibre.com.ar/MLA-658751354-tester-digital-multimetro-inicial-escuela-onda-cuadrada-t830-_JM) para el taller. 

Los talleristas proveerán algunos para el taller pero para la continuidad de los proyectos es necesario que los participantes puedan tener acceso en su institución o laboratorio.


# Costo

Costo total del taller sin kits (no más de 20 personas).

Costo del kit. $ (aproximado). Se necesitan al menos dos semanas de anticipación para gestionar su compra.


# Contacto

Fernando Castro. 2615734372. [ferhcastro@gmail.com](mailto:ferhcastro@gmail.com)
